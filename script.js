// ===========================================VARIABLES=====================================

let nom = document.getElementById('nom');
let nbPaires = document.getElementById('nbPaires');

// regular expressions
let modelNom = /^[a-z0-9 ]+$/i;
let modelPaires = /2|3|4|5|6|7|8|9|10/;

let form = document.getElementById('form');
let erreurs = document.getElementById('erreurs');
let jeu = document.getElementById('jeu');
let fin = document.getElementById('fin');

// variables pour identifier et différencier les cartes tournées
let aTourneCarte = false;
let carte1;
let carte2;

// le tableau contient toutes les cartes en double
let tableauCartes = [
    'img/01.png',
    'img/01.png',
    'img/02.png',
    'img/02.png',
    'img/03.png',
    'img/03.png',
    'img/04.png',
    'img/04.png',
    'img/05.png',
    'img/05.png',
    'img/06.png',
    'img/06.png',
    'img/07.png',
    'img/07.png',
    'img/08.png',
    'img/08.png',
    'img/09.png',
    'img/09.png',
    'img/10.png',
    'img/10.png'
    ]

form.addEventListener('submit', valider);
jeu.style.display = 'none';
document.getElementById('chrono').style.display = 'none';



// ===========================================FONCTIONS=====================================


function valider(e) {
    let messages = [];
    if (!modelNom.test(nom.value)) {
        messages.push('Vous devez entrer un nom valide');
    }
    if (!modelPaires.test(nbPaires.value)) {
        messages.push('Vous devez entrer un nombre de paires entre 2 et 10')
    }

    // afficher messages d'erreur et empêcher sumbit
    if (messages.length > 0) {
        e.preventDefault();
        // joindre le message d'erreur à une nouvelle ligne
        erreurs.innerText = messages.join('\n');
    } else {
        // cacher le formulaire, générer le jeu et partir le chronomètre
        document.getElementById('form').style.display = 'none';
        document.getElementById('erreurs').style.display = 'none';
        generer();
        chrono ();
    }
}


function generer() {
    // affucher div jeu
    jeu.style.display = 'block';
    // obtenir nbCartes et ajuster tableauCartes.lenght en conséquence
    let nbPaires = document.getElementById('nbPaires').value;
    let nbCartes = nbPaires * 2;
    tableauCartes.length = nbCartes;

    // Mélanger cartes de tableauCartes
    for (let i = 0; i < nbCartes; i++) {
        let aleatoire = Math.floor(Math.random() * (nbCartes));
        [tableauCartes[i], tableauCartes[aleatoire]] = [tableauCartes[aleatoire], tableauCartes[i]];
    }

    // Créer un bouton pour chaque carte
    for (let i = 0; i < tableauCartes.length; i++){
        // créer un élément
        let carte = document.createElement('button');
        // donner du contenu à l'élément
        let imageCarte = document.createElement('img');
        imageCarte.src = tableauCartes[i];
        carte.appendChild(imageCarte);
        // ajouter l'élément au DOM
        jeu.appendChild(carte);
        // cacher l'image
        carte.classList.add('back');
        // ajouter un attribut à chaque carte pour pouvoir comparer les attributs
        carte.setAttribute('numero', tableauCartes[i]);
        // ajouter un eventListener sur chaque carte
        carte.addEventListener('click', tourner);
    }
}


function tourner () {
    // changer classe des cartes cliquées
    this.classList.replace('back', 'front');
    // empêcher de cliquer la même carte deux fois
    this.removeEventListener('click', tourner);

    // changer valeur de aTourneCarte pour pouvoir répéter la fonction tourner() avec la prochaine carte 
    if (aTourneCarte === false) {
        // pousser première carte cliquée dans variable carte1
        carte1 = this;
        aTourneCarte = true;
        return;
    }   

    // pousser deuxième carte cliquée dans variacle carte2
    carte2 = this;
    aTourneCarte = false;    
    
    comparer();
}

// comparer les cartes 1 et 2 
function comparer () {
    if (carte1.getAttribute('numero') == carte2.getAttribute('numero')) {

        nbPaires.value -= 1;        
        carte1.removeEventListener('click', tourner);
        carte2.removeEventListener('click', tourner);
        carte1.classList.replace('front', 'transparent');
        carte2.classList.replace('front', 'transparent');

        // fin de la partie
        if (nbPaires.value == 0) {
            victoire();
            clearTimeout(t);
        }

        return;
    }
    
    if (carte1.getAttribute('numero') !== carte2.getAttribute('numero')) {
        setTimeout(retourner, 800);
        // bloquer le jeu temporairement
        jeu.classList.add('stop');

            function retourner(){
                // débloquer le jeu
                jeu.classList.remove('stop');
                carte1.classList.replace('front', 'back');
                carte2.classList.replace('front', 'back');
                carte1.addEventListener('click', tourner);
                carte2.addEventListener('click', tourner);
            }
    }
}

// temps restant à la partie
function chrono () {
    // fonction qui est déclenchée si le temps est écoulé
    t = setTimeout(defaite, 120000);

    let temps = 120;
    let sec = temps % 60;
    let min = Math.floor(temps / 60);  
    if (sec < 10) {
        sec = '0' + sec;
    } 
    document.getElementById('chrono').style.display = 'block';
    document.getElementById('chrono').textContent = min + ':' + sec;
    document.getElementById('chrono').setAttribute('data-sec', temps);
    let intervale = setInterval(moins1Sec, 1000);
    return intervale;

    function moins1Sec () {
        let temps = document.getElementById('chrono').getAttribute('data-sec');
        temps = parseInt(temps);
        temps -= 1;
        let sec = temps % 60;
        let min = Math.floor(temps / 60); 
        if (sec < 10) {
            sec = '0' + sec;
        } 
        document.getElementById('chrono').textContent = min + ':' + sec;
        document.getElementById('chrono').setAttribute('data-sec', temps);
        if(temps == 0){
            clearInterval(intervale);
        }
    }     
} 

// afficher message de victoire et arrêter le chronomètre
function victoire () {
    document.getElementById('chrono').setAttribute('data-sec', 1);
    let finDiv = document.createElement('div');
    finDiv.classList.add('bravo');
    let finTexte = document.createTextNode('Bravo ' + nom.value + '! Vous avez réussi!');
    finDiv.appendChild(finTexte);
    fin.appendChild(finDiv);
    rejouer ();
}

// afficher le message de défaite et cacher le jeu
function defaite () {
    jeu.style.display = 'none';
    let finDiv = document.createElement('div');
    finDiv.classList.add('desole');
    let finTexte = document.createTextNode('Désolé ' + nom.value + '! Vous avez perdu!');
    finDiv.appendChild(finTexte);
    fin.appendChild(finDiv);
    rejouer ();
}

// afficher le bouton pour rafraîchir le jeu
function rejouer (){
    let rejouerDiv = document.createElement('button');
    rejouerDiv.classList.add('rejouer');
    let rejouerTexte = document.createTextNode('Rejouer');
    rejouerDiv.appendChild(rejouerTexte);
    fin.appendChild(rejouerDiv);
    rejouerDiv.addEventListener('click', refresh)
}

function refresh() {
    window.location.reload();
}



